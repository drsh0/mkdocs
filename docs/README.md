![hero](assets/images/hero.png)

Welcome to **Cyber Security: An Offensive Mindset**, UTS MIDAS Summer Studio 2019.

!!! note
    Thank you to everyone that participated in Summer Studio 2019. This course has now reached its conclusion for 2019. The course material will still be made available on this website. All enquiries are to be directed towards contact (at) utscyber.org. 

## Overview :tada:

This studio aims to expose students to real-world offensive security practices. Students will experience common and unorthodox attack vectors and learn to simulate the mindset of a threat actor. Unlike standard learning environments, this studio will provide hands-on and lab-focused experiences that will ingrain students with the practical and foundation knowledge needed to embark on a potential career in cyber security. Students will have the opportunity to present their final project to industry professionals at the end of the studio. Students from all listed faculties are welcome.

## Staff :busts_in_silhouette:

#### Larry Zektser

#### Jai Ghorpade

#### Luke Fuehrer

#### Darsh Shah

## About this site :grey_question:

This wiki will be the place for all documentation, lesson resources, and for FAQs. Please be sure to visit this wiki often. We suggest you start with understanding the layout of the studio and browse the FAQ for any unanswered questions. 

Official subject information and contact details will be made available on [UTS Online](https://online.uts.edu.au/) and on MS Teams. 


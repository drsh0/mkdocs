## **Let's break into machines!**
## Boot2root 0x0

[^1]![offsec, try harder](https://i.postimg.cc/BZztbvcp/Try-Harder-Flames-Web-Page-Banner-Smaller-1.png)

!!! info
    This may seem like a huge jump from playing wargames, CTFs, various other challenges, and vulnernable web apps. However, we hope to demo as much of the process as possible which, in conjunction with your own research and discoveries, will lead to a meaningful base to continue your pentesting adventures. 
    
## **Deliverables**

!!! important
    Be sure to read every part of these deliverables. These are critical to you passing the subject. Be sure to message us on MS Teams if you have any concerns or if you can see any discrepencies in the details below.
    
### Wednesday, 20/02/2018 - **GROUP**

* Present a case study on one or more tool(s), and how these tool(s) can be used to break into complex and vulnerable systems.
    * Brownie points for linking your research to the guest (TBA) presentation that will be held in the morning (10AM - 12PM).
    * Make sure to include examples. 
    * Keep it high-level. 
    * Your time will be limited (see below):
* Presentations are to be 6 to 7 minutes. No less, no more.
* Presentations will be conducted in groups.
* Presentations will be held between 1PM and 2PM.
* Your research is to be submitted individually to your sprint 3 submission. Be sure to include your reflection on your learning whilst linking it back to the SLOs. Do not forget your references, academic resources and artefacts.
* In your reflection, make sure that your research **is** technical and low-level.


### Friday, 22/02/2018 - **INDIVIDUAL**

!!! Work in Progress
    _please note we are still fleshing this deliverable out_

**_OPTION 1_: Own a vulnerable machine of your choice**

* VulnHub (see resources below)
* HTB - Retired
* [Cyber Security Challenge Australia - in-a-box](https://www.cyberchallenge.com.au/#thechallenge)
* [TryHackMe](https://tryhackme.com) (one of the boxes)
* Any other boot 2 root images you find (please reach out if you are unsure)

**AND / OR**

**_OPTION 2_: Register to the <REDACTED> real-world test program**

* Reminder: the journey is always greater than the destination
* Students are expected to be able to explain:
    * Why they did what they did
    * Tools they used
    * Methodologies

### Sunday, 24/02/2018 - **INDIVIDUAL**

* This submission should be a full-fledged reflection of this week (18/02 - 22/02):
    * Reflect on your learnings from Monday to Friday, be sure to be inclusive of the face-to-face hours and the work you have conducted outside of these hours.
    * Write about your free-4-all reflection(s) and what you have learnt from each other throughout the week.
* Artefacts need to be shown, such as, but not limited to, screenshots, resources, academic references, videos, selfies in class and presentations slides.
* If you don’t know already (which you should), click here to see how to submit your weekly submission.


-------------

## Vulnerable VM Resources

### Hypervisors

Please choose a hypervisor of your choice. We recommend VMWare Workstation (Win/Linux) or VMWare Fusion (macOS) (free for FEIT students) or Virtualbox (x-platform, free). 

#### Networking 

* The general setup includes using an attacker VM (e.g. Kali Linux, Parrot, etc) and victim VM/s that are vulnerable and are to be attacked. If both VMs are on the same NAT network, they should be able to communicate and also reach the internet (filtered). 

* For reverse shells you may need to set up port forwarding and/or check your firewalls. 

* Alternatively, if you are using your host OS as the attacker, then a host only system will also be adequate for the victim VM/s to be on. 

* For more troubleshooting please refer to [Vulnhub networking](https://www.vulnhub.com/lab/network/)

### Vulnerable VMs

* [Abatchy's fantastic list of vulnerable VMs for all skill levels](https://www.abatchy.com/2017/02/oscp-like-vulnhub-vms.html)

* [Ippsec's fantastic retired hackthebox walkthroughs -- make notes!](https://www.youtube.com/channel/UCa6eh7gCkpPo5XXUDfygQQA)

    * Please note that hackthebox retired boxes are only made available to VIP users (£10/m). You don't have to sign up to this as there are more than enough free vulnerable VMs available. Additionally, feel free to use the above videos and walkthroughs for reference. 


### Group Presentation Resources

### Some cool tools to research

* Metasploit

* Web app scanners; Nikto, WPScan, SQLmap, Dirbuster, burp suite, BeEF

* Passwords: john the ripper, hashcat

* Network: routersploit, nmap, dnsrecon, aircrack, kismet

* much much more! 

Many more cool tools available [here](https://github.com/enaqx/awesome-pentest)


[^1]: Credit to offensive security - https://www.offensive-security.com/when-things-get-tough/.

## How to find more about offensive security tools? 


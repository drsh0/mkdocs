## Sprint 1 
*Due:* Sun 10/2/19, 11PM on UTSOnline (see FAQ on how to submit your reflections)

*Files:* All files that you may need for submission are available [here](https://teams.microsoft.com/_#/files/General?threadId=19%3A0659dac2203f4309be1a4d9b180858d7%40thread.skype&ctx=channel&context=Week%25201) (redirects to MS Teams)

* Complete personal reflection for each SLO;
* Complete group reflection (best to do that after Friday group meeting);
* Submissions must contain:
    * **Introduction**
    
        * A photo and statement of motivation to take the studio subject and objectives in taking the subject

    * **Learning Contract**
    
        * What do you intend to achieve through the project, including learning goals and tasks to be completed. 
        * What will be your contribution to the group effort? What will you learn in that process? What will you produce? 
        * You may revise this in subsequent submissions.
    
* Live static site OR some evidence of set up in progress e.g. git repo, research on static site generators; and
* Artefacts: presentations slides, content etc.

## Security News :newspaper:
* [/r/netsecstudents](https://www.reddit.com/r/netsecstudents)
* [/r/netsec](https://www.reddit.com/r/netsec/)
* [https://www.darkreading.com/](https://www.darkreading.com/)
* [https://stewpolley.com/recommended-reading/](https://stewpolley.com/recommended-reading/)
* [SwiftOnSecurity](https://twitter.com/swiftonsecurity)

## Security Videos :vhs:
* [LiveOverflow](https://www.youtube.com/channel/UClcE-kVhqyiHCcjYwcpfj9w)
* [DEFCON Videos - Largest Security Conference in the World](https://www.youtube.com/user/defconvidoes)
* [John Hammond](https://www.youtube.com/user/RootOfTheNull)
* [Computerphile](https://www.youtube.com/channel/UC9-y-6csu5WGm29I7JiwpnA) (focus on cryptography and protocols)

## Static site :globe_with_meridians:

The studio wants to encourage students to create a working static site to host your weekly reflections, submissions, and to ultimately post your portfolio which is due in week 4. 
 
### Intro :arrow_right:
* **Dynamic** sites require the use of server side scripting that increases the possibilities of vulnerabilities and attacks. Whilst dynamic sites are required for many web applications, static sites are gaining popularity for their simplicity and for their increased security
* **Static** sites are presented using plain HTML with no server side scripting required. This usually requires the use of a static site generator that is able to take input files written in markup (e.g. Markdown) 

### Resources :green_book:
* [https://www.staticgen.com/](https://www.staticgen.com/)
* [https://netlify.com](https://netlify.com)
* Getting Started guides for your chosen static site generators e.g. Jekyll, Hugo, Gatsby, Hexo
* If you get stuck: [https://github.com/poole/poole](https://github.com/poole/poole)
* [Github Pages](https://help.github.com/articles/using-jekyll-as-a-static-site-generator-with-github-pages/) or [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/) (make sure you browse some examples)
## **Web Application Security**

## Resources

Your task this week is to develop your web penetration skills with the many resources available to help you understand vulnerable web apps and the methodology required to break into them. Here are a list of intentionally vulnerable web apps that you can use to practice these tasks. 

* [Web for Pentester](https://www.pentesterlab.com/exercises/web_for_pentester/)
* [WebGoat](https://www.owasp.org/index.php/Category%3aOWASP_WebGoat_Project)
* [Natas](http://overthewire.org/wargames/natas/)
* [Shellter Labs](https://shellterlabs.com/en/)
* [Root Me](https://www.root-me.org/?lang=en)

## Homework

### **Due**: immediately

* Define your problem statement ASAP.

We encourage studio members to set an achievable goal and problem statement that allows them to build their skills within web app security testing and allows you to utilise the various resources available online. You should be working towards the sprint continuously and refining your portfolio. You should have content available for scrums. 
 
### **Due**: Wed 11/02
**Don't forget that your static website is due Wed 11/02**

`Individual task?` **`No`**`, groups are welcome.`

Research Responsible Disclosure and Bug Bounties:

* [https://www.hackerone.com/disclosure-guidelines](https://www.hackerone.com/disclosure-guidelines)

* [https://www.bugcrowd.com/resource/what-is-responsible-disclosure/](https://www.bugcrowd.com/resource/what-is-responsible-disclosure/)

Write **300-500** words with artefacts (screenshots, video(s), diagram(s), bibliography, academic articles etc.):

* How ethical hacking and bug bounties are impacting stakeholders
* How you can implement things you've learnt throughout the week to test web apps under a responsible disclosure program

`Are you presenting?` **`No`**`. You will be uploading your homework to your static website.`

### **Due:** Fri 15/02
`Individual task?` **`Yes`**

1. Have your defined problem statement on web-application security

2. Use your problem statement and the resources provided above to show evidence of web-app vulnerabilities. Were you able to manipulate the content? Where you able to escalate privileges on the web-app? How does it tie back to your problem statement? Have you linked your outcomes and results to any of the SLOs?

3. More importantly, how do these issues effect a business? How can businesses remediate these issues? How effective are security prevention systems and software development life-cycles?
 
`Are you presenting? You should be` **`ready to present`** `as you will be asked randomly in class, however it is not mandatory. You will be uploading your web-pen test findings/journey and artefacts to your static website.`

### Design Thinking
As this subject is about design thinking, think about a problem that you want to solve. This can be personal such as being able to understand various web attacks in the context of various modern vulnerabilities that stakeholders face.
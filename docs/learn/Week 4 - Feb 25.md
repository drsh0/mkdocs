## Reverse Engineering && boot2root 0x1

This week, we continue where we left off last week and attempt to break into virtual environments without prior write-ups available (ongoing).

Additionally, there will be a focus on reverse engineering that students should explore and reflect on. 

## Monday 25/2/19

### Reverse Engineering

!!! tip
    Don't fear -- reverse engineering is not as scary as it sounds! This will be a introduction to the concepts reverse engineering binaries. Whilst it might hard to grasp at first, keep trying!

We will be visited by an industry professional that will introduce us to the world of reverse engineering.

#### Preparation

!!! note "Please install the following on your OS of choice before attending class"

* [Binary Ninja Demo](https://binary.ninja/demo/) -- installers for this can be found at the bottom of the page. Additioanlly, 

* Set up an attack VM just in case (e.g. Kali). 

* Introductory Video: 

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/QXjU9qTsYCc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



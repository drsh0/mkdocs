# Frequently Asked Questions

??? faq "What days and time does the studio run?"
    * Mon, Wed, Fri
    * 10AM - 2PM (with 1 hour break)

        !!! info
            Please note that on Monday 4th Feb (the first day of the studio), there is a launch party from 9AM - 12PM, and a pizza lunch from 12PM - 1PM. 1PM - 5PM is dedicated to the first studio session where we will meet and greet and discuss the week's work

??? faq "What is the census date?"
    ==Thu 7 Feb 2019==
    
??? faq "Where can I find the subject outline?"
    The subject outline is available at [UTS Online](https://online.uts.edu.au) or via MS Teams

??? faq "Where do I submit my weekly sprints?"
    Please submit all sprints to the specified folder within UTS Online. If you have any questions please reach out to us on MS Teams and please keep a lookout on the MS Teams channels.

??? faq "How much time is expected to be allocated to this studio?"
    Since this is a 6cp subject, students are expected to put in a total of 34 hours a week including face to face classes. In other words, **25 hours per week** are expected for summer studio activities. 

??? faq "Will teaching staff be available to consult during the week?"
    Absolutely. We firmly believe providing as much contact time as possible for students to help them grow and learn. Exact timing and availabilities will be made available at a later date. 

# Setup

## Virtual Machine Lab Setup

### Requirements

To complete this course satisfactorily operating virtual machines (VM) will be required. Never played around with a VM? Fear not, this guide is for you!

* [ ] Laptop to run VMs and perform tasks and/or utilise lab computers
* [ ] Hypervisor software]
    * [ ] VmWare Workstation (free for students) or;
    * [ ] Virtualbox (free)
* [ ] Adequate disk space (minimum 30GB)

Rest assured that lab machines have been configured to allow students to practice and learn on if your personal devices do not meet the requirements above and/or are not suited for virtualisation. 

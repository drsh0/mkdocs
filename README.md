# Gitlab Repo for UTS Cyber Summer Studio 2019 (utscyber.studio)

This repo contains the raw markup files, a CI config using Gitlab CI (as seen below) and a modified Mkdocs config and theme setup (mkdocs material). To contribute please focus on the `docs` folder. Any files modified, added or removed will result in a rebuild and redeploy of the static site. 

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: python:alpine

before_script:
  - pip install mkdocs
  # add your custom theme (https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes) if not inside a theme_dir
  # - pip install mkdocs-material

pages:
  script:
  - mkdocs build
  - mv site public
  artifacts:
    paths:
    - public
  only:
  - master
```

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install](http://www.mkdocs.org/#installation) MkDocs
1. Preview your project: `mkdocs serve`
1. Add content
1. Generate the website: `mkdocs build` (optional)

Read more at MkDocs's [documentation](http://www.mkdocs.org/).
